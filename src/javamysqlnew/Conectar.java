/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package javamysqlnew;
import java.sql.*;

public class Conectar {
    public static final String URL = "jdbc:mysql://localhost:3306/proyecto";
    public static final String USER = "root";
    public static final String CLAVE = "Mysqlcontra123";
    
    public Connection getConexion(){
        Connection con = null;
        try{
           Class.forName("com.mysql.cj.jdbc.Driver");
           con = (Connection) DriverManager.getConnection(URL, USER, CLAVE);
           System.out.println("Conectado");
        }
        catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        return con;
    }    
}
