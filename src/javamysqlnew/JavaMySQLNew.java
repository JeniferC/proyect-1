/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package javamysqlnew;
import java.sql.*;

public class JavaMySQLNew {

    public static void main(String[] args) {
        Conectar conecta = new Conectar();
        Connection con = conecta.getConexion();
        
        String consulta="SELECT * FROM clientes";
        
        try {
            Statement sentencia = con.createStatement();
            ResultSet resultado=sentencia.executeQuery(consulta);
            
            while (resultado.next()){
                System.out.println ("Codigo: " + resultado.getInt (1) + ", Nombre: " + resultado.getString(2));
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
